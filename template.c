#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


#define ascanf(format, ...) \
    do { \
        int32_t _ = scanf(format, __VA_ARGS__); \
        assert(_); \
    } while (0);


int main(void) {
    return 0;
}