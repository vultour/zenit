#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


struct chocolate_t {
    uint64_t value;
    struct chocolate_t *next;
};

struct chocolate_t* cmalloc(uint64_t value) {
    struct chocolate_t *node = malloc(sizeof(struct chocolate_t));
    assert(node != NULL);
    node->value = value;
    node->next = NULL;
    return node;
}

int try_swap_next_larger(struct chocolate_t *this, uint64_t new) {
    if (this->next->value == new) {
        return 1;
    }
    if (this->next->value > new) {
        struct chocolate_t *tmp = this->next;
        this->next = cmalloc(new);
        this->next->next = tmp;
        return 2;
    }
    return 0;
}

int main(void) {
    int _;
    int N = 0;
    struct chocolate_t *head = NULL;

    _ = scanf("%d", &N);
    assert(_);

    int i;
    uint64_t x;
    for (i = 0; i < N; i++) {
        _ = scanf("%ld", &x);
        assert(_);

        if (i < 1) {
            head = cmalloc(x);
            continue;
        }
        
        struct chocolate_t *current = head;
        if (head->value == x) {
            continue;
        }
        if (head->value > x) {
            struct chocolate_t *tmp = head;
            head = cmalloc(x);
            head->next = tmp;
            continue;
        } else {
            while (current->next != NULL) {
                if (try_swap_next_larger(current, x)) {
                    break;
                }
                current = current->next;
            }
            if (current->next == NULL) {
                current->next = cmalloc(x);
            }
        }
    }

    struct chocolate_t *current = head;
    uint64_t c = 0;
    while (current != NULL) {
        c += 1;
        current = current->next;
    }

    printf("%ld\n", c);

    return 0;
}
