#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define NAME_MAX_SIZE 11


int main(void) {
    int32_t _;
    int32_t N;
    _ = scanf("%d");
    assert(_);

    int32_t i;
    char* name[NAME_MAX_SIZE];
    int32_t v;
    int32_t t;
    int32_t z;
    int32_t s;
    int32_t c;
    int32_t k;
    int32_t p;
    for (i = 0; i < N; i++) {
        uint64_t time = 0;
        _ = scanf(
            "%s %d %d %d %d %d %d %d",
            &name, &v, &t, &z, &s, &c, &k, &p
        );
        assert(_ == 8);

        int32_t boil_round;
        for (boil_round = z; boil_round > 0; --boil_round) {
            time += ((v / z) / 100) * 3 * (t - 23);
        }

        int32_t drink_round = v;
        while (drink_round > 0) {
            int32_t amount;
            if (drink_round >= 247) {
                amount = 247;
            } else {
                amount = drink_round;
            }
            time += amount * k * (t - c);
        }
    }

    return 0;
}
