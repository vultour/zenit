#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


int main(void) {
    int32_t _;
    int32_t N;
    uint64_t l;
    int32_t d;

    _ = scanf("%d %ld %d", &N, &l, &d);
    assert(_);

    int32_t i;
    int32_t x;
    uint64_t win_time = 0;
    int32_t win_i = 0;
    for (i = 0; i < N; i++) {
        _ = scanf("%d", &x);
        assert(_);

        if (i < 1) {
            win_time = (l / x);
            win_i = i;
            continue;
        }

        uint64_t time = l / x;
        if (win_time > time) {
            win_time = time;
            win_i = i;
        }
    }

    printf("%d\n", win_i + 1);
    return 0;
}