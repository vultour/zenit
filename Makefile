CFLAGS := -Wall -O2

SRCS := $(wildcard *.c)
PRGS := $(patsubst %.c,%,$(SRCS))
OBJS := $(patsubst %,%.o,$(PRGS))
BINS := $(patsubst %,bin/%,$(PRGS))

all: bin $(BINS)

bin:
	mkdir -p bin

bin/% : $(OBJS)
	$(CC) $(patsubst bin/%,%.o,$@) -o $@

clean:
	$(RM) $(BINS)
	rmdir bin
