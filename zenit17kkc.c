#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


struct coin_t {
    int32_t value;
    struct coin_t *next;
};

struct coin_t* cmalloc(int32_t value) {
    struct coin_t *node = malloc(sizeof(struct coin_t));
    node->value = value;
    node->next = NULL;
    return node;
}

int try_swap_next(struct coin_t *this, int32_t value) {
    if (this->next->value <= value) {
        struct coin_t *tmp = this->next;
        this->next = cmalloc(value);
        this->next->next = tmp;
        return 1;
    }
    return 0;
}

int main(void) {
    int32_t _;
    int32_t M;
    int32_t s;

    _ = scanf("%d %d", &M, &s);
    assert(_);

    int32_t i;
    int32_t x;
    struct coin_t *head = NULL;
    for (i = 0; i < M; i++) {
        _ = scanf("%d", &x);
        assert(_);

        if (i < 1) {
            head = cmalloc(x);
            continue;
        }

        if (head->value <= x) {
            struct coin_t *tmp = head;
            head = cmalloc(x);
            head->next = tmp;
            continue;
        }

        struct coin_t *current = head;
        while (current->next != NULL) {
            if (try_swap_next(current, x)) {
                break;
            }
            current = current->next;
        }
        if (current->next == NULL) {
            current->next = cmalloc(x);
        }
    }

    int32_t sum = 0;
    i = 0;
    struct coin_t *current = head;
    while (current != NULL) {
        i += 1;
        sum += current->value;
        if (sum >= s) {
            printf("%d\n", i);
            return 0;
        }
        current = current->next;
    }

    printf("-1\n");
    return 0;
}