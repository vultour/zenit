#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


int main(void) {
    int _;
    int N = 0;
    int64_t sum = 0;
    int cnt = 0;

    _ = scanf("%d", &N);
    if (_ < 1) {
        printf("Error");
        exit(1);
    }

    int x;
    int i;
    for (i = 0; i < N; i++) {
        _ = scanf("%d", &x);
        if (_ < 1) {
            printf("Error");
            exit(1);
        }
        if (x >= 0) {
            sum += x;
            cnt += 1;
        }
    }

    if (cnt <= 0) {
        printf("Ziadne nezaporne cisla\n");
        return 0;
    }

    printf("Sucet cisel: %ld\n", sum);
    printf("Priemer cisel: %ld\n", sum / cnt);
    return 0;
}
