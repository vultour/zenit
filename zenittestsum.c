#include <stdlib.h>
#include <stdio.h>


int main(void) {
    int _;
    int N = 0;
    int result = 0;

    _ = scanf("%d", &N);
    if (_ < 1) {
        printf("Error");
        exit(1);
    }

    int i;
    int x;
    for (i = 0; i < N; i++) {
        _ = scanf("%d", &x);
        if (_ < 1) {
            printf("Error");
            exit(1);
        }
        result += x;
    }

    printf("%d\n", result);
    return 0;
}
